from helpers import retry_on_error


@retry_on_error()
def get_all_osm_master_routes_using_agency_name(overpass_api, agency_name):
    response = overpass_api.query("""
        [out:json];
        relation["type"="route_master"]["gtfs:agency_name"="{0}"];
        out;
        // &contact=https://gitlab.com/stalker314314/prostorne-jedinice-import/
    """.format(agency_name))
    return response.relations


@retry_on_error()
def get_all_osm_master_routes_using_operator(overpass_api, operator):
    response = overpass_api.query("""
        [out:json];
        relation["type"="route_master"]["operator"="{0}"];
        out;
        // &contact=https://gitlab.com/stalker314314/prostorne-jedinice-import/
    """.format(operator))
    return response.relations


@retry_on_error()
def get_osm_routes(overpass_api, master_route_id):
    response = overpass_api.query("""
        [out:json];
        rel({master_route_id});
        rel(r)->.routes;
        (
          .routes;
          >;
        )->.parts;
        (
          .routes;
          .parts;
        );
        out;
        // &contact=https://gitlab.com/stalker314314/prostorne-jedinice-import/
    """.format(master_route_id=master_route_id))
    return response


def get_all_non_operator_stops(overpass_api, operator):
    response = overpass_api.query("""
        [out:json];
        (
          nwr[highway=bus_stop][operator="{operator}"];
          nwr[highway=bus_stop][operator="{operator}"];
          nwr[railway=tram_stop][operator="{operator}"];
        )->.all_stops;
        (
          relation[route=bus][operator="{operator}"];
          relation["disused:route"=bus][operator="{operator}"];
          relation[route=tram][operator="{operator}"];
          relation["disused:route"=tram][operator="{operator}"];
          relation[route=trolleybus][operator="{operator}"];
          relation["disused:route"=trolleybus][operator="{operator}"];
        )->.all_routes;
        (
          .all_routes;
          >>;
        )->.all_from_relations;
        (
          nwr.all_from_relations[highway=bus_stop][operator="{operator}"];
          nwr.all_from_relations[railway=tram_stop][operator="{operator}"];
        )->.all_stops_from_relations;
        (
          .all_stops; - .all_stops_from_relations;
        );
        out;
    """.format(operator=operator))
    return response